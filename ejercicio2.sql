﻿DROP DATABASE IF EXISTS p1ej2;
CREATE DATABASE IF NOT EXISTS p1ej2;
USE p1ej2;

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  peso int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE compran(
  idclientes int,
  idproductos int,
  fecha date,
  cantidad int,
  PRIMARY KEY(idclientes,idproductos)
);

ALTER TABLE compran
  ADD CONSTRAINT fkcompranproductos FOREIGN KEY(idproductos) REFERENCES productos(id),
  ADD CONSTRAINT fkcompranclientes FOREIGN KEY(idclientes) REFERENCES clientes(id),
  ADD CONSTRAINT ukcompranproductos UNIQUE KEY(idproductos);